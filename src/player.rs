use serde::{Serialize,Deserialize};

#[derive(PartialEq, Eq, Debug, Serialize, Deserialize, Copy, Clone)]
pub enum PlayerAction {
    Ready,
    Attack,
    PowerAttack,
    Heal,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Player {
    pub id: u64,
    pub nick: String,
    pub hp: u64,
    pub combo: u64,
    pub action: PlayerAction
}

impl Player {
    pub fn new(nick: String, id: u64) -> Player {
        Player {
            id,
            nick,
            hp: 100,
            combo: 0,
            action: PlayerAction::Ready
        }
    }

    pub fn get_id(&self) -> u64 {
        self.id
    }

    pub fn get(&self) -> Player {
        self.clone()
    }

    pub fn set(&mut self, state: Player) {
        self.nick = state.nick;
        self.hp = state.hp;
        self.combo = state.combo;
        self.action = state.action;
    }

    pub fn set_action(&mut self, action: PlayerAction) {
        self.action = action;
    }

    pub fn get_action(&self) -> &PlayerAction {
        &self.action
    }
}