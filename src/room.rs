use rand::random;
use serde::{Serialize, Deserialize};
use crate::player::{Player, PlayerAction};

const ATK_NORMAL: u64 = 10;
const ATK_NORMAL_RND: u64 = 20;
const ATK_POWER: u64 = 30;
const ATK_POWER_RND: u64 = 20;
const HEAL: u64 = 30;

#[derive(PartialEq, Eq, Debug, Copy, Clone, Serialize, Deserialize)]
pub enum RoomAction {
    Awaiting,
    PlayerOne,
    PlayerTwo,
    Finished
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Room {
    pub player_1: Option<Player>,
    pub player_2: Option<Player>,
    pub action: RoomAction,
}

impl Room {
    pub fn new() -> Room {
        Room {
            player_1: None,
            player_2: None,
            action: RoomAction::Awaiting
        }
    }

    pub fn reset(&mut self) {
        self.action = RoomAction::Awaiting;
        self.player_1 = None;
        self.player_2 = None;
    }

    pub fn update(&mut self) -> Room {
        let mut state = self.get();

        match state.action {
            RoomAction::Awaiting => {
                if state.player_1.is_some() && self.player_2.is_some() {
                    state.action = RoomAction::PlayerOne;
                }
            }
            RoomAction::PlayerOne => {
                if let Some(p1) = &mut state.player_1 {
                    if let Some(p2) = &mut state.player_2 {
                        if p1.action == PlayerAction::Ready {
                            return state;
                        }
                        match p1.action {
                            PlayerAction::Ready => {}
                            PlayerAction::Heal => {
                                if p1.combo < 3 {return state};
                                p1.hp += HEAL;
                                p1.combo = 0;
                            }
                            PlayerAction::Attack => {
                                let power: u64 = ATK_NORMAL + (random::<u64>() % ATK_NORMAL_RND);
                                p2.hp = if p2.hp > power { p2.hp - power } else { 0 };
                                p1.combo += 1;
                            }
                            PlayerAction::PowerAttack => {
                                if p1.combo < 3 {return state};
                                let power: u64 = ATK_POWER + (random::<u64>() % ATK_POWER_RND);
                                p2.hp = if p2.hp > power { p2.hp - power } else { 0 };
                                p2.combo = 0;
                                p1.combo = 0;
                            }
                        }
                        if p2.hp > 0 {
                            state.action = RoomAction::PlayerTwo;
                        } else {
                            state.action = RoomAction::Finished;
                        }
                    }
                }
            }
            RoomAction::PlayerTwo => {
                if let Some(p1) = &mut state.player_1 {
                    if let Some(p2) = &mut state.player_2 {
                        if p2.action == PlayerAction::Ready {
                            return state;
                        }
                        match p2.action {
                            PlayerAction::Ready => {}
                            PlayerAction::Heal => {
                                if p2.combo < 3 {return state};
                                p2.hp += HEAL;
                                p2.combo = 0;
                            }
                            PlayerAction::Attack => {
                                let power: u64 = ATK_NORMAL + (random::<u64>() % ATK_NORMAL_RND);
                                p1.hp = if p1.hp > power { p1.hp - power } else { 0 };
                                p2.combo += 1;
                            }
                            PlayerAction::PowerAttack => {
                                if p2.combo < 3 {return state};
                                let power: u64 = ATK_POWER + (random::<u64>() % ATK_POWER_RND);
                                p1.hp = if p1.hp > power { p1.hp - power } else { 0 };
                                p2.combo = 0;
                                p1.combo = 0;
                            }
                        }
                        if p1.hp > 0 {
                            state.action = RoomAction::PlayerOne;
                        } else {
                            state.action = RoomAction::Finished;
                        }
                    }
                }
            }
            RoomAction::Finished => {}
        }

        let rs = state.clone();

        if let Some(p1) = &mut state.player_1 {
            if let Some(p2) = &mut state.player_2 {
                p1.action = PlayerAction::Ready;
                p2.action = PlayerAction::Ready;
            }
        }
        self.set(state.clone());
        rs
    }

    pub fn join(&mut self, player: Player) -> bool {
        return if self.player_1.is_none() {
            println!("Joined as player 1");
            self.player_1 = Some(player);
            true
        } else if self.player_2.is_none() {
            println!("Joined as player 2");
            self.player_2 = Some(player);
            true
        } else {
            println!("Room full.");
            false
        }
    }

    pub fn get(&self) -> Room {
        self.clone()
    }

    pub fn set(&mut self, state: Room) {
        if let Some(p1state) = state.player_1 {
            if let Some(p1) = &mut self.player_1 {
                p1.set(p1state);
            }
        }
        if let Some(p2state) = state.player_2 {
            if let Some(p2) = &mut self.player_2 {
                p2.set(p2state);
            }
        }
        self.action = state.action;
    }
}