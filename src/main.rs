mod player;
mod room;
mod server;

use std::sync::Arc;
use std::{thread, time};
use std::collections::HashMap;
use std::sync::atomic::{AtomicBool, Ordering};

use crate::room::{Room, RoomAction};
use crate::player::{Player,PlayerAction};
use crate::server::{Message, MessageType, Server, ServerError};

fn main() {
    let mut rooms: HashMap<String, Room> = HashMap::new();
    //let mut room = Room::new();
    let mut server = Server::new("0.0.0.0:5000");

    let ctrlc = Arc::new(AtomicBool::new(false));
    let ctrlc_handler = ctrlc.clone();

    ctrlc::set_handler(move || {
        ctrlc_handler.store(true, Ordering::SeqCst);
    }).expect("Cannot handle CTRL+C");

    server.run();



    while !ctrlc.load(Ordering::SeqCst) {
        while let Some((id, msg)) = server.recv() {
            match msg.id {
                MessageType::PlayerJoin => {
                    let player = Player::new(msg.nick.clone(), id);
                    if let Some(room) = rooms.get_mut(msg.room.as_str()) {
                        if !room.join(player) {
                            server.send(id, Message::error(ServerError::RoomFull));
                        }
                    } else {
                        let mut room = Room::new();
                        room.join(player);
                        rooms.insert(msg.room, room);
                    }
                }
                MessageType::PlayerExit => {
                    println!("Player exit!");
                    server.disconnect(id);
                }
                MessageType::PlayerAttack => {
                    if let Some(room) = rooms.get_mut(msg.room.as_str()) {
                        if let Some(player) = &mut room.player_1 {
                            if player.get_id() == id {
                                player.set_action(PlayerAction::Attack);
                            }
                        }

                        if let Some(player) = &mut room.player_2 {
                            if player.get_id() == id {
                                player.set_action(PlayerAction::Attack);
                            }
                        }
                    }
                }
                MessageType::PlayerPowerAttack => {
                    if let Some(room) = rooms.get_mut(msg.room.as_str()) {
                        if let Some(player) = &mut room.player_1 {
                            if player.get_id() == id {
                                player.set_action(PlayerAction::PowerAttack);
                            }
                        }

                        if let Some(player) = &mut room.player_2 {
                            if player.get_id() == id {
                                player.set_action(PlayerAction::PowerAttack);
                            }
                        }
                    }
                }
                MessageType::PlayerHeal => {
                    if let Some(room) = rooms.get_mut(msg.room.as_str()) {
                        if let Some(player) = &mut room.player_1 {
                            if player.get_id() == id {
                                player.set_action(PlayerAction::Heal);
                            }
                        }

                        if let Some(player) = &mut room.player_2 {
                            if player.get_id() == id {
                                player.set_action(PlayerAction::Heal);
                            }
                        }
                    }
                }
                _ => {}
            }
        }

        for (_, room) in rooms.iter_mut() {
            let prev_state = room.get();
            let cur_state = room.update();

            if cur_state.action != prev_state.action {
                println!("Old state {:?}, new {:?}", prev_state.action, cur_state.action);
                match cur_state.action {
                    RoomAction::Awaiting => {
                        println!("Room state: Awaiting");
                    }
                    _ => {
                        println!("Room state: Other");
                        if let Some(p1) = &cur_state.player_1 {
                            server.send(p1.id, Message::state(cur_state.clone()));
                        }
                        if let Some(p2) = &cur_state.player_2 {
                            server.send(p2.id, Message::state(cur_state.clone()));
                        }
                        if cur_state.action == RoomAction::Finished {
                            room.reset();
                        }
                    }
                }
            }
        }

        //thread::yield_now();
        thread::sleep(time::Duration::from_millis(1));
    }
}