use std::net::{TcpListener, TcpStream};
use std::sync::{Arc, mpsc, Mutex};
use std::{thread, time};
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread::JoinHandle;
use serde_json::{json, Value};
use tungstenite::WebSocket;
use serde::{Serialize,Deserialize};
use crate::Room;

#[derive(Debug, Eq, PartialEq)]
pub enum MessageType {
    PlayerJoin,
    PlayerExit,
    PlayerAttack,
    PlayerPowerAttack,
    PlayerHeal,
    ServerState,
    ServerError
}

#[derive(Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum ServerError {
    ConnectionClosed,
    RoomFull
}

#[derive(Debug, Serialize, Deserialize)]
pub enum MessagePayload {
    String(String),
    Number(u64),
    State(Room),
    Error(ServerError)
}

#[derive(Debug)]
pub struct Message {
    pub id: MessageType,
    pub nick: String,
    pub room: String,
    pub payload: Option<MessagePayload>
}

impl Message {
    pub fn join(nick: String, room: String) -> Message {
        Message {
            id: MessageType::PlayerJoin,
            nick,
            room,
            payload: None
        }
    }

    pub fn exit(nick: String, room: String) -> Message {
        Message {
            id: MessageType::PlayerExit,
            nick,
            room,
            payload: None
        }
    }

    pub fn attack(nick: String, room: String) -> Message {
        Message {
            id: MessageType::PlayerAttack,
            nick,
            room,
            payload: None
        }
    }

    pub fn power_attack(nick: String, room: String) -> Message {
        Message {
            id: MessageType::PlayerPowerAttack,
            nick,
            room,
            payload: None
        }
    }

    pub fn heal(nick: String, room: String) -> Message {
        Message {
            id: MessageType::PlayerHeal,
            nick,
            room,
            payload: None
        }
    }

    pub fn state(state: Room) -> Message {
        Message {
            id: MessageType::ServerState,
            nick: "SYSTEM".to_string(),
            room: "".to_string(),
            payload: Some(MessagePayload::State(state))
        }
    }

    pub fn error(error: ServerError) -> Message {
        Message {
            id: MessageType::ServerError,
            nick: "SYSTEM".to_string(),
            room: "".to_string(),
            payload: Some(MessagePayload::Error(error))
        }
    }
}

struct ConnectionIdGenerator {
    next_id: u64
}

impl ConnectionIdGenerator {
    pub fn new() -> ConnectionIdGenerator {
        ConnectionIdGenerator{
            next_id: 0
        }
    }

    pub fn get_next(&mut self) -> u64 {
        println!("Generated ID: {}", self.next_id);
        let i = self.next_id;
        self.next_id += 1;
        i
    }
}

struct Connection {
    id: u64,
    nick: String,
    rx: Option<mpsc::Receiver<Message>>,
    tx: Option<mpsc::Sender<Message>>,
    socket: Arc<Mutex<WebSocket<TcpStream>>>,
    thread_send: Option<JoinHandle<()>>,
    thread_recv: Option<JoinHandle<()>>,
    dead: Arc<AtomicBool>
}

impl Connection {
    pub fn new(socket: WebSocket<TcpStream>, id: u64) -> Connection {
        Connection {
            id,
            nick: String::new(),
            rx: None,
            tx: None,
            socket: Arc::new(Mutex::new(socket)),
            thread_send: None,
            thread_recv: None,
            dead: Arc::new(AtomicBool::new(false))
        }
    }

    pub fn set_nick(&mut self, nick: String) {
        self.nick = nick;
    }
    pub fn get_nick(&self) -> String {
        self.nick.clone()
    }

    pub fn get_id(&self) -> u64 {
        self.id
    }

    pub fn is_dead(&self) -> bool {
        self.dead.load(Ordering::Relaxed)
    }

    pub fn send(&mut self, msg: Message) {
        if self.is_dead() {
            return
        }
        if let Some(tx) = &self.tx {
            tx.send(msg).unwrap();
        }
    }

    pub fn flush(&mut self) {
        if let Ok(mut socket) = self.socket.lock() {
            if socket.can_write() {
                socket.write_pending().unwrap();
            }
        }
    }

    pub fn recv(&mut self) -> Option<Message> {
        if let Some(rx) = &self.rx {
            if let Ok(msg) = rx.try_recv() {
                if msg.id == MessageType::PlayerJoin {
                    self.set_nick(msg.nick.clone());
                }
                return Some(msg);
            }
        }
        None
    }

    pub fn run(&mut self) {
        let socket_tx = Arc::clone(&self.socket);
        let socket_rx = Arc::clone(&self.socket);

        let (my_tx, thread_rx) = mpsc::channel::<Message>();
        let (thread_tx, my_rx) = mpsc::channel::<Message>();

        self.tx = Some(my_tx);
        self.rx = Some(my_rx);

        //Receiver
        let dead = Arc::clone(&self.dead);
        self.thread_recv = Some(thread::spawn(move || {
            let socket = socket_rx;
            while !dead.load(Ordering::Relaxed) {
                if let Ok(mut socket) = socket.try_lock() {
                    if socket.can_read() {
                        if let Ok(packet) = socket.read_message() {
                            if packet.is_text() {
                                if let Ok(msg) = serde_json::from_str::<Value>(packet.to_text().unwrap()) {
                                    if let Value::String(nick) = &msg["nick"] {
                                        if let Value::String(packet_type) = &msg["type"] {
                                            match packet_type.as_str() {
                                                "player_join" => {
                                                    if let Value::String(room) = &msg["room"] {
                                                        println!("Player join: {}", &nick);
                                                        thread_tx.send(Message::join(nick.clone(), room.clone())).unwrap();
                                                    }
                                                }
                                                "player_exit" => {
                                                    if let Value::String(room) = &msg["room"] {
                                                        println!("Player exit: {}", &nick);
                                                        thread_tx.send(Message::exit(nick.clone(), room.clone())).unwrap();
                                                    }
                                                }
                                                "player_attack" => {
                                                    if let Value::String(room) = &msg["room"] {
                                                        println!("Player attack: {}", &nick);
                                                        thread_tx.send(Message::attack(nick.clone(), room.clone())).unwrap();
                                                    }
                                                }
                                                "player_powerattack" => {
                                                    if let Value::String(room) = &msg["room"] {
                                                        println!("Player power attack: {}", &nick);
                                                        thread_tx.send(Message::power_attack(nick.clone(), room.clone())).unwrap();
                                                    }
                                                }
                                                "player_heal" => {
                                                    if let Value::String(room) = &msg["room"] {
                                                        println!("Player heal: {}", &nick);
                                                        thread_tx.send(Message::heal(nick.clone(), room.clone())).unwrap();
                                                    }
                                                }
                                                _ => {}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                thread::sleep(time::Duration::from_millis(1));
            }
        }));

        let dead = Arc::clone(&self.dead);
        self.thread_send = Some(thread::spawn(move || {
            let socket = socket_tx;
            while !dead.load(Ordering::Relaxed) {
                for packet in &thread_rx {
                    let mut output = Value::Null;

                    match packet.id {
                        MessageType::PlayerJoin => {
                            output = json!({
                                "type": "player_join",
                                "nick": packet.nick,
                                "room": packet.room
                            })
                        }
                        MessageType::PlayerExit => {
                            output = json!({
                                "type": "player_exit",
                                "nick": packet.nick,
                                "room": packet.room
                            })
                        }
                        MessageType::PlayerAttack => {
                            output = json!({
                                "type": "player_attack",
                                "nick": packet.nick,
                                "room": packet.room
                            })
                        }
                        MessageType::PlayerPowerAttack => {
                            output = json!({
                                "type": "player_powerattack",
                                "nick": packet.nick,
                                "room": packet.room
                            })
                        }
                        MessageType::PlayerHeal => {
                            output = json!({
                                "type": "player_heal",
                                "nick": packet.nick,
                                "room": packet.room
                            })
                        }
                        MessageType::ServerState => {
                            if let Some(MessagePayload::State(v)) = packet.payload {
                                output = json!({
                                    "type": "server_state",
                                    "nick": "SYSTEM",
                                    "state": v
                                })
                            }
                        }
                        MessageType::ServerError => {
                            if let Some(MessagePayload::Error(e)) = packet.payload {
                                output = json!({
                                    "type": "server_error",
                                    "nick": "SYSTEM",
                                    "error": e
                                })
                            }
                        }
                    }
                    if let Ok(mut socket) = socket.lock() {
                        socket.write_message(tungstenite::Message::Text(output.to_string())).unwrap();
                        if packet.id == MessageType::PlayerExit {
                            socket.close(None).unwrap();
                            dead.store(true, Ordering::Relaxed);
                        }
                    }
                }
            }
        }));
    }
}

impl Drop for Connection {
    fn drop(&mut self) {
        self.flush();
        self.dead.store(true, Ordering::Relaxed);
    }
}

pub struct Server {
    //rx: mpsc::Receiver<Message>,
    //tx: mpsc::Sender<Message>,
    //thread_rx: mpsc::Receiver<Message>,
    //thread_tx: mpsc::Sender<Message>,
    listener: Arc<Mutex<TcpListener>>,
    thread: Option<JoinHandle<()>>,
    clients: Arc<Mutex<Vec<Connection>>>,
    dead: Arc<AtomicBool>

}

impl Server {
    pub fn new(addr: &str) -> Server {
        let listener = TcpListener::bind(addr).expect("Cannot bind!");
        //listener.set_nonblocking(true).unwrap();
        Server {
            //rx: my_rx,
            //tx: my_tx,
            //thread_rx: server_rx,
            //thread_tx: server_tx,
            listener: Arc::new(Mutex::new(listener)),
            thread: None,
            clients: Arc::new(Mutex::new(Vec::new())),
            dead: Arc::new(AtomicBool::new(false))
        }
    }

    pub fn send(&self, client_id: u64, msg: Message) {
        if let Ok(mut clients) = self.clients.lock() {
            for client in clients.iter_mut() {
                if client.get_id() == client_id {
                    client.send(msg);
                    break;
                }
            }
        }
    }

    pub fn recv(&self) -> Option<(u64, Message)> {
        if let Ok(mut clients) = self.clients.try_lock() {
            for client in clients.iter_mut() {
                if let Some(msg) = client.recv() {
                    return Some((client.get_id(), msg));
                }
            }
        }
        None
    }

    pub fn flush(&mut self) {
        if let Ok(mut clients) = self.clients.lock() {
            for client in clients.iter_mut() {
                client.flush();
            }
        }
    }

    pub fn disconnect(&mut self, id: u64) {
        if let Ok(mut clients) = self.clients.lock() {
            if let Some(idx) = clients.iter().position(|c| c.get_id() == id) {
                let client = clients.get_mut(idx).unwrap();
                client.send(Message::error(ServerError::ConnectionClosed));
                clients.remove(idx);
                println!("Disconnected: {}", id);
            }
        }
    }

    pub fn run(&mut self) {
        let listener = Arc::clone(&self.listener);
        let clients = Arc::clone(&self.clients);
        let dead = Arc::clone(&self.dead);
        self.thread = Some(thread::spawn(move || {
            let mut idgen = ConnectionIdGenerator::new();
            while !dead.load(Ordering::Relaxed) {
                if let Ok((stream, _)) = listener.lock().unwrap().accept() {
                    stream.set_read_timeout(Some(time::Duration::from_millis(10))).unwrap();
                    println!("New connection from: {}", stream.peer_addr().unwrap());
                    if let Ok(stream) = tungstenite::accept(stream) {
                        let mut client = Connection::new(stream, idgen.get_next());
                        client.run();
                        clients.lock().unwrap().push(client);
                    }
                }
            }
        }));
    }

    pub fn stop(&mut self) {
        self.dead.store(true, Ordering::Relaxed);
        if let Ok(mut clients) = self.clients.lock() {
            while let Some(mut client) = clients.pop() {
                client.send(Message::error(ServerError::ConnectionClosed));
                println!("Disconnected: {}", client.get_id());
            }
        }
        /*if let Some(thread) = self.thread.borrow() {
            thread.join().unwrap();
        }*/
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        self.flush();
        self.stop();
    }
}
